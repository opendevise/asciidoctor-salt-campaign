= Asciidoctor funding campaign

Together, we're working to shape the future of writing and publishing, particularly in the field of technical documentation.
We want people to use open source not just because it's the best software, but because it's the best _documented_ software.

We've come a long way.
We have even bigger plans for the future.
To continue making our vision a reality, we need to obtain funding for Asciidoctor.

Sarah and I have setup an account for Asciidoctor on BountySource's new funding platform, https://github.com/bountysource/frontend/wiki/Salt-Frequently-Asked-Questions[Salt], to ensure the ongoing success and sustainability of the Asciidoctor project.
(BountySource is like Kickstarter for open source projects).
We also want to be completely transparent about how the project is funded.
You can read more details about this campaign and how it works on the https://salt.bountysource.com/teams/asciidoctor[Asciidoctor campaign page].

Salt will enable us to accept financial backing for Asciidoctor from both organizations and individuals and allocate the money the way we think best suits the project.
The initial goal is to allow Sarah and I to work on Asciidoctor full time.

Asciidoctor has grown and matured substantially over the last three years.
It's now a critical component of documentation, especially in open source.
In order to keep up with the project's rate of growth, manage the Asciidoctor ecosystem as a whole, and be there to facilitate participation, we need to be able to dedicate more time and attention to this work.
We also need to cover current and future infrastructure costs.
The more money this campaign raises (on an ongoing basis), the more we can achieve, the more possibilities we can unlock and the greater the impact Asciidoctor can have.

By securing funding for Asciidoctor, Sarah and I can focus on keeping the Asciidoctor project operating smoothly and releasing components more frequently.
We love running the Asciidoctor project and we love the community.
//That's why we're seeking funding.
We want to continue serving the community and doing what we love to make a toolchain writers have always wanted.

*Let's make it happen!*

If you have questions or concerns, please don't hesitate to respond.
We'll give you an opportunity to comment before we announce the campaign on other channels.
