<h1>Asciidoctor <small>Write in AsciiDoc, Publish Everywhere</small></h1>

---

<p class="lead">We’re working to shape the future of writing and publishing, primarily in the field of technical documentation.
To achieve this vision, <a href="https://salt.bountysource.com/checkout/amount?team=asciidoctor&reward_id=29">we need your support</a>.</p>

We know we’re already doing something right because **authors who use Asciidoctor love it**.

> Now that Asciidoctor is around, I can do truly amazing things with my AsciiDoc source.
>
> <footer>Scott Chacon, author of Pro Git: <em><a href="https://medium.com/@chacon/living-the-future-of-technical-writing-2f368bd0a272">Living the Future of Technical Writing</a></em></footer>

We want to take it further.
We want writers to love writing.
We want them to love the writing experience.
As writers ourselves, we’re committed to doing whatever we can to help writers publish with confidence!

<h2>What is Asciidoctor?</h2>

The [Asciidoctor](http://asciidoctor.org) project is a comprehensive, user-friendly publishing toolchain for the AsciiDoc syntax.
The Asciidoctor community drives the AsciiDoc syntax forward by introducing new ideas and innovations and promoting it through education and advocacy.

<dl>
<dt>AsciiDoc is…​</dt>
<dd><p>a semantic and presentation-agnostic text-based writing format for authoring content.
AsciiDoc incorporates numerous text markup conventions and provides semantics that map closely to schemas like DocBook.</p></dd>
<dt>Asciidoctor is…​</dt>
<dd><p>a <em>fast</em>, open source text processor and publishing toolchain for converting AsciiDoc content to HTML5, PDF, EPUB3, DocBook 5, LaTeX, man pages and other formats.
Asciidoctor offers APIs for extending the AsciiDoc language and for transforming the parsed document.</p></dd>
</dl>

> Asciidoctor provides a docs engine that enables us to write and refine content like any other source code.
> Our team members and community alike are able to contribute to the upstream source.
>
> <footer>N. Harrison Ripps</footer>

<h2>Our vision</h2>

**<big>We want to empower writers.</big>**

Far too many writers are suffocated by so-called writing tools.
They get tangled in XML or burdened with WYSIWYG workflows that spit out cruft and make editing tedious.
The process is stifling writing and, as a result, our collective knowledge.
We want to change all that.

We want to shrink the gap between thinking, writing and publishing.
We want a practical, open source toolchain that amplifies writers’ creativity instead of crushing it.
We want to transform simple, adaptable content—​cleanly separated from presentation thanks to AsciiDoc—​into beautiful, professional-quality results.

> [Using AsciiDoc,] writers can focus on content over styling, but still have absolute control over how the data is organized and presented.
>
> <footer>N. Harrison Ripps</footer>

Encoding your content in AsciiDoc is more than just an easier, more efficient way to write documentation.
It maximizes the value of your content. You can
**write in AsciiDoc and publish everywhere!**

<h3>Docs = Code</h3>

Coding and writing are alike in many ways.
They’re both forged by carefully weaving structure and creativity.
Both go through many revisions on their journey towards quality.
Despite these similarities, coders get all the amazing tools while the writers are left with scraps.

It’s time to realize that documentation is code too and, thus, deserves the same treatment.

> Nobody but professional doc writers like writing in XML.
> If the goal is to get more people, then [embrace] non-XML.
>
> <footer>Steven Citron-Pousty</footer>

We need to give writers more powerful tools that enable them to work how and where they want, efficiently.
Navigation, refactoring, static analysis, references.
Writers need IDEs too.
**Let’s make it happen!**

<h2>What we’ve achieved so far</h2>

In only three years, the Asciidoctor project has matured from a single Ruby script to a community of nearly [200 people](http://asciidoctor.org/contributors/) contributing to roughly [50 individual repositories](https://github.com/asciidoctor/).
Asciidoctor is now the leading implementation of AsciiDoc.

Asciidoctor can transform AsciiDoc into a myriad of output formats, including:

* HTML5
* DocBook 5
* PDF
* EPUB3
* LaTeX
* man pages
* Mallard
* HTML-based slideshows
* …​and more

Asciidoctor is available for many programming languages, including:

* Ruby (MRI, JRuby, Rubinius)
* JavaScript (using Asciidoctor.js, transpiled from Ruby using Opal)
* Java, Groovy and any other language that runs on the JVM (using AsciidoctorJ, a Java API that leverages JRuby underneath)

Live HTML preview of AsciiDoc, powered by Asciidoctor.js, is available in:

* Browsers (Chrome, Firefox)
* Text editors (Atom, Brackets)
* IDEs (Eclipse, IntelliJ, NetBeans)

See [this list](https://plus.google.com/114112334290393746697/posts/C1QLRBynXTB) for a summary of (most of) the repositories in the Asciidoctor ecosystem to give you a better sense of what we’ve been able to achieve so far.

<h2>What we want to achieve</h2>

<h3>Short term</h3>

1. **Full-time dedication to the Asciidoctor project**

  There are high expectations for Asciidoctor.
  We want to focus on meeting them.

  1. We must complete the issues scheduled for [Asciidoctor 1.5.3](https://github.com/asciidoctor/asciidoctor/milestones/v1.5.3) and get that version released.
  1. Next, we need to get Asciidoctor PDF out of alpha and the [Asciidoctor PDF 1.5.0](https://github.com/asciidoctor/asciidoctor-pdf/milestones/v1.5.0) release published.
  1. Finally, we need to start on [Asciidoctor 1.6.0](https://github.com/asciidoctor/asciidoctor/milestones/v1.6.0) so we can get it released and integrated into AsciidoctorJ.

  <br>
  Every release of Asciidoctor (core) involves overseeing the release of all the components that depend on it.
The aforementioned work should easily keep us busy for the duration of 2015.

1. **Create professional-quality PDF output (driven by a simple theming system)**

  We’d like to continue our work on Asciidoctor PDF so it becomes the easiest way to create branded, professional-quality PDFs.
  There’s still work to do to enhance and simplify the theming system.
  We also want to make the converter extensible so it can be used in specialty applications.

1. **Create professional-quality EPUB3 output**

  We’d like to accelerate the development of Asciidoctor EPUB3 so it becomes the easiest way to create branded, professional-quality EPUBs.
  There’s still work to do to accommodate different document types (book, publication, document, etc).
  (Currently, the output is biased towards magazine publications).

1. **Add STEM (aka math) support to PDF and EPUB3**

  Related to the previous two items, we need to add STEM support to the PDF and EPUB3 converters.
  We currently leverage the MathJax library in the HTML converter, which interprets LaTeX and AsciiMath on the client.
  However, both PDF and EPUB3 require the equations be preprocessed before inserting them into the document.
  We want this feature to work out of the box so we can accommodate the needs of the scientific community.

1. **Enhance the documentation**

  Documentation is so important, yet ours is still lacking.
  We need to write more complete guides, tutorials, manuals and reference documentation.

1. **Establish a visual identity**

  A mature project deserves a strong brand.
  We want to create a visual identity to represent the Asciidoctor brand in the following forms:

  * logo
  * logo type
  * icon
  * color schema

  <br>
  These assets will be used for the project website, listings in software catalogs and for UI components inside tools.

<h3>Longer term</h3>

1. **Relaunch the project website**

  The current project website was setup to handle the Asciidoctor project when it was only a single repository.
  With nearly 50 projects in the Asciidoctor ecosystem, the messaging, design and workflow used for the website is not able to keep up.
  We need to relaunch the site with a much large ecosystem in mind, re-architect the user experience and automate more of the workflow.
  In particular, the website needs to automatically incorporate the documentation from each of the individual projects.

1. **Experiment with tooling**

  As mentioned in “Our Vision”, writers deserve better tooling.
  We want to help drive, and hopefully fund, the tooling effort in the Asciidoctor ecosystem.
  This involves spending time in design meetings, evaluating prototypes, performing user studies, providing direction and motivation for contributors and facilitating collaboration.
  Lots of attention is needed here and we want to be able to provide it.

1. **Parse inline content into an AST**

  Asciidoctor does not yet parse inline content during the parse phase.
  This is a critical limitation for extension writers and also results in some unintended side-effects.
  We want to switch to parsing inline content during the parse phase (see [#61](https://github.com/asciidoctor/asciidoctor/issues/61)).
  As a positive consequence, this switch from regular expression-based substitution to a parser will help establish a formal grammar for the inline components of the AsciiDoc syntax.
  This change may also get us started on the AsciiDoc specification (aka “UniDoc”).

1. **Convert to semantic and multi-page HTML5**

  The HTML5 output from Asciidoctor is far from semantic.
  It was designed to comply with the HTML5 backend from AsciiDoc Python.
  The web has since moved forward and so must our HTML5 converter.
  We need to switch to a highly semantic HTML5 document (see [#242](https://github.com/asciidoctor/asciidoctor/issues/242)) and also consider how best to produce multiple pages from a single AsciiDoc source (see [#626](https://github.com/asciidoctor/asciidoctor/issues/626)).

_If there are other objectives you’d like us to prioritize, please let us know via the issue tracker for the respective [project](https://github.com/asciidoctor)._

<h2>Why Asciidoctor needs your support</h2>

Despite all that’s been accomplished, there’s still a lot of work to do in Asciidoctor.
We certainly can’t do it alone, and we can only make so much progress working after hours.
Some improvements will be very tough to implement without a dedicated commitment.
We also need to cover current and future infrastructure costs.

In addition to programming tasks, we keep the community running on a day-to-day basis.
We answer dozens of posts per day on the discussion list, issue tracker and social networks.
We spend time reviewing and merging pull requests, monitoring code changes and ensuring the overall quality of the project.
We onboard new users and developers and give feedback to other developers on new innovations and prototypes.
Occasionally, we get out to speak about it ;)

We *love* playing these roles, and we’re effective at doing them.
When we have support, we’re able to focus on doing this work with an abundance of energy and enthusiasm and without disruption.

**You can make this happen by pledging an amount each month.**

By backing Asciidoctor, you’re funding our time and allowing us to focus on making this vision a reality.
The more money this campaign raises (on an ongoing basis), the more we can achieve, the more possibilities we can unlock and the greater the impact Asciidoctor can make.

You’ll also be funding a role model in open source.
We are committed to open source and we cultivate a friendly community where everyone feels welcome.

<h2>Before we get back to hacking…​</h2>

We want to give writers like you the toolchain you’ve always wanted.
Together, we can bring tremendous value to open source documentation by optimizing for creativity.
Please consider funding the Asciidoctor project today.
**Thank you!**

---
